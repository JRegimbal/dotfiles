filetype plugin indent on
syntax on

" Spaces for shift, default to 4
set expandtab
set tabstop=4
set shiftwidth=4

" Airline
let g:airline_theme = 'papercolor'
let g:airline_powerline_fonts = 1

" NerdTree
nnoremap <C-n> :NERDTreeToggle<CR>
