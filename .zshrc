## Antigen (so zsh is nice)
source $HOME/antigen.zsh
antigen use oh-my-zsh
antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle lukechilds/zsh-nvm

antigen theme decoxviii/dotfiles data/zsh/custom/themes/nox

antigen apply

## Alias and environment variables
alias ls="ls --color=auto"
export EDITOR=vim
# Rustup files
export PATH="$PATH:$HOME/.cargo/bin"
# Gem binaries
export PATH="$PATH:$HOME/.gem/ruby/2.5.0/bin"

## Prompt
#PROMPT=$'%T %B%3~%b $(git_prompt_info)%{$reset_color%}\n%n%# '
#RPROMPT='%(?..%F{red}[%?]%f)'

## Functions
# gitignore.io API
function gi() { curl -L -s https://www.gitignore.io/api/$@ ;}

# Update vim 8 packages
function vim-upgrade() {
    (cd $HOME/.vim/pack/packages/start;
    for f in `ls -A`; do;
        if [[ -d $f ]]; then;
            cd $f;
            git pull;
            git submodule update --recursive;
            cd ..;
        fi;
    done)
}

# Create PDFs in directories using pandoc
function makepdf() {
    typeset -i FAILURES=0
    typeset -i SUCCESSES=0
    typeset -i ALREADY=0

    for file in **/*.md; do
        cd "$(dirname "$file")"
        echo "Converting \"${file}\"."
        FILENAME="$(basename "$file")"
        # Check if markdown file is older than PDF
        if [ "$FILENAME" -ot "${FILENAME%.md}.pdf" ]; then
            ALREADY+=1
        else
            pandoc "$FILENAME" -o "${FILENAME%.md}.pdf"
            STATUS=$?
            if [ $STATUS = 0 ]; then
                SUCCESSES+=1
            else
                FAILURES+=1
            fi
        fi
        cd ..
    done
    echo ""
    echo "$SUCCESSES successes, $FAILURES failures, and $ALREADY already converted."

    if [ $FAILURES -eq 0 ]; then
        return 0
    else
        return 1
    fi
}
